

# Install/Upgrade mishkal from pip
# pip3 install -r requirements.txt -t mishkallo/pythonpath/ --upgrade

# If extension was installed, remove first
# unopkg remove mishkallo
rm -f mishkal.oxt
zip -r mishkal.oxt *.xcu build.sh description.xml LICENSE README.md requirements.txt pkg-description registration icons mishkallo.py pythonpath META-INF Office dialogs
# unopkg add -f "mishkallo.oxt"
# soffice --norestore
