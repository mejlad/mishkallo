# -*- coding: utf_8 -*-

import uno
import unohelper
import re
import os
import sys
import mishkal.tashkeel as t
import pyarabic.araby as araby
from collections import Counter
from com.sun.star.frame import XDispatchProvider
from com.sun.star.frame import XDispatch
from com.sun.star.frame import FeatureStateEvent
from com.sun.star.frame import ControlCommand
from com.sun.star.lang import XInitialization
from com.sun.star.beans import PropertyValue, NamedValue
from com.sun.star.task import XJob
from com.sun.star.awt import XMouseListener
from com.sun.star.awt import XAdjustmentListener
from com.sun.star.awt import XTextListener
from com.sun.star.xml.sax import XDocumentHandler
from com.sun.star.awt.PosSize import X, Y, HEIGHT, WIDTH, POSSIZE, SIZE


# v = sys.version_info[:2]
# if 'win' or 'nt' in sys.platform:
#     found = os.path.join(os.environ['LOCALAPPDATA']+'\Programs\Python')
#     if os.path.exists(found):
#         python = [p for p in os.listdir(found) if p[6:][1] == str(v[1])]
#         if python:
#             url = os.path.join(found + f"\{python[0]}")
#             sys.path.insert(0, url)

def RGB(R,G,B):
    packed = int('%02x%02x%02x' % (R, G, B), 16)
    return packed

def hex_to_rgb(hex_color):
    return tuple(int(hex_color.strip("#")[i:i+2], 16) for i in (0, 2, 4))

def createUnoService(ctx, service_name):
    try:
        service = ctx.ServiceManager.createInstanceWithContext(service_name, ctx)
    except:
        service = None
    return service


def makeNamedValue(name, value):
    aNamedValue = NamedValue()
    aNamedValue.Name = name
    aNamedValue.Value = value
    return aNamedValue


class Tashkel(unohelper.Base, XJob):
    def __init__(self, ctx):
        self.ctx = ctx
        self.auto = self.read_settings()

    def read_settings(self):
        with open(f"{os.path.dirname(__file__)}/dialogs/settings.txt", "r") as settings:
            lines = settings.readlines()
            auto = int(lines[0][lines[0].index(":")+1:-1])
            return auto

    def save_space(self, s: str) -> str:
        space = ''
        for i in s:
            if i.isspace():
                space = space + i
            else:
                break
        return space

    def tashkeel(self, text: str) -> str:
        start = self.save_space(text)
        end = self.save_space(text[::-1])
        vocalizer = t.TashkeelClass()
        result = vocalizer.tashkeel(text)
        return f"{start}{result.strip()}{end}"

    def color_safety(self, counter_char, counter_tashkel, selection):
        start = selection.Text.createTextCursorByRange(selection.Start)
        end = selection.Text.createTextCursorByRange(selection.End)
        while selection.Text.compareRegionEnds(start, end) > 0:
            start.goRight(1, True)
            if araby.is_tashkeel(start.String):
                counter_tashkel[start.CharColor] += 1
            else:
                counter_char[start.CharColor] += 1
            start.goRight(0, False)
        return counter_char, counter_tashkel

    def execute(self, args):
        desktop = self.ctx.ServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", self.ctx)
        doc = desktop.getCurrentComponent()
        controller = doc.CurrentController
        view_cursor = doc.CurrentController.ViewCursor
        search_descriptor = doc.createSearchDescriptor()
        counter_tashkel = Counter()
        counter_char = Counter()
        if doc.CurrentController.Selection.supportsService("com.sun.star.text.TextTableCursor"):
            table = doc.getTextTables().getByName(doc.CurrentController.ViewCursor.TextTable.Name)
            rangeName = doc.CurrentController.Selection.getRangeName()
            for i in range(int(rangeName[1]), int(rangeName[4]) + 1):
                for x in range(ord(rangeName[0]), ord(rangeName[3]) + 1):
                    cell = table.getCellByName(f'{chr(x)}{i}')
                    text = re.sub(araby.FIX_SPACES_PAT, ' ', cell.String)
                    text_without_tashkel = araby.strip_tashkeel(text)
                    result = self.tashkeel(text_without_tashkel)
                    togather = [(k, v) for k, v in zip(text.split(), result.split()) if araby.vocalizedlike(k, v)]

                    start = cell.createTextCursorByRange(cell.Start)
                    end = cell.createTextCursorByRange(cell.End)
                    self.color_safety(counter_char, counter_tashkel, cell)

                    count = len(togather)
                    start.goRight(0, False)
                    for word in togather:
                        search_descriptor.SearchString = word[0]
                        found = doc.findNext(start, search_descriptor)
                        if found:
                            found.CharColor = counter_char.most_common(1)[0][0]
                            found.setString(word[1])
                        count -= 1
            if counter_tashkel:
                job = ColorTashkelJob(self.ctx)
                job.execute((counter_tashkel.most_common(1)[0][0]))
            if self.auto:
                job = ColorTashkelJob(self.ctx)
                job.execute(())
        if doc.CurrentController.Selection.supportsService("com.sun.star.text.TextRanges"):
            text = ''.join(i.String for i in doc.CurrentController.Selection)
            text = re.sub(araby.FIX_SPACES_PAT, ' ', text)
            # if araby.is_vocalizedtext(text):
            #     print("is_vocalizedtext")
            text_without_tashkel = araby.strip_tashkeel(text)
            result = self.tashkeel(text_without_tashkel)
            togather = [(k, v) for k, v in zip(text.split(), result.split()) if araby.vocalizedlike(k, v)]

            selection = controller.getSelection().getByIndex(0)
            start = selection.Text.createTextCursorByRange(selection.Start)
            end = selection.Text.createTextCursorByRange(selection.End)
            self.color_safety(counter_char, counter_tashkel, selection)
            is_not_start = start.goLeft(1, True)
            count = len(togather)
            start.goRight(0, False)
            for word in togather:
                search_descriptor.SearchString = word[0]
                found = doc.findNext(selection.Start, search_descriptor)
                if found:
                    found.CharColor = counter_char.most_common(1)[0][0]
                    found.setString(word[1])
                    # found.CharColor = 15926533
                count -= 1
                if count == 0:
                    if is_not_start:
                        break
                    start.gotoRange(start.Start, False)
                    # start.collapseToStart()
                    start.gotoPreviousWord(True)
                    start.gotoRange(end, True)
                    controller.select(start)
            if counter_tashkel:
                job = ColorTashkelJob(self.ctx)
                job.execute((counter_tashkel.most_common(1)[0][0]))
            if self.auto:
                job = ColorTashkelJob(self.ctx)
                job.execute(())


class RemoveTashkelJob(unohelper.Base, XJob):
    def __init__(self, ctx):
        self.ctx = ctx

    def color_safety(self, counter, selection):
        start = selection.Text.createTextCursorByRange(selection.Start)
        end = selection.Text.createTextCursorByRange(selection.End)
        while selection.Text.compareRegionEnds(start, end) > 0:
            start.goRight(1, True)
            counter[start.CharColor] += 1
            start.goRight(0, False)
        return counter.most_common(1)[0][0]

    def execute(self, args):
        desktop = self.ctx.ServiceManager.createInstanceWithContext(
            "com.sun.star.frame.Desktop", self.ctx)
        doc = desktop.getCurrentComponent()
        controller = doc.CurrentController
        view_cursor = doc.CurrentController.ViewCursor
        search_descriptor = doc.createSearchDescriptor()
        search_descriptor.SearchString = "|".join(araby.TASHKEEL)
        search_descriptor.SearchRegularExpression = True
        if controller.Selection.supportsService("com.sun.star.text.TextTableCursor"):
            table = doc.getTextTables().getByName(doc.CurrentController.ViewCursor.TextTable.Name)
            rangeName = doc.CurrentController.Selection.getRangeName()
            for i in range(int(rangeName[1]), int(rangeName[4]) + 1):
                for x in range(ord(rangeName[0]), ord(rangeName[3]) + 1):
                    cell = table.getCellByName(f'{chr(x)}{i}')
                    start = cell.Text.createTextCursorByRange(cell.Start)
                    end = cell.Text.createTextCursorByRange(cell.End)
                    counter = Counter()
                    color = self.color_safety(counter, cell)
                    if doc.Text.compareRegionEnds(start, end) >= 0:
                        start.goRight(0, False)
                        found = doc.findNext(start, search_descriptor)
                        while found:
                            if doc.Text.compareRegionEnds(found, end) == -1:
                                break
                            found.CharColor = color
                            if found.CharColor != color:
                                found.setPropertyValue("CharColor", color)
                            found.String = ""
                            found = doc.findNext(found.End, search_descriptor)
        if controller.Selection.supportsService("com.sun.star.text.TextRanges"):
            selection = doc.getCurrentController().getSelection().getByIndex(0)
            start = selection.Text.createTextCursorByRange(selection.Start)
            end = selection.Text.createTextCursorByRange(selection.End)
            counter = Counter()
            color = self.color_safety(counter, selection)
            if doc.Text.compareRegionEnds(start, end) >= 0:
                start.goRight(0, False)
                # cursor.gotoStart(False)
                found = doc.findNext(start, search_descriptor)
                while found:
                    if doc.Text.compareRegionEnds(found, end) == -1:
                        break
                    found.CharColor = color
                    if found.CharColor != color:
                        found.setPropertyValue("CharColor", color)
                    found.String = ""
                    found = doc.findNext(found.End, search_descriptor)
        return ()


class ColorTashkelJob(unohelper.Base, XJob):
    def __init__(self, ctx):
        self.ctx = ctx

    def execute(self, args):
        CharColor = -1
        with open(f"{os.path.dirname(__file__)}/dialogs/settings.txt", "r") as settings:
            lines = settings.readlines()
            CharColor = int(lines[1][lines[1].index(":")+1:-1])
        if args:
            CharColor = args
        desktop = self.ctx.ServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", self.ctx)
        doc = desktop.getCurrentComponent()
        controller = doc.CurrentController
        view_cursor = doc.CurrentController.ViewCursor
        search_descriptor = doc.createSearchDescriptor()
        search_descriptor.SearchString = "|".join(araby.TASHKEEL)
        search_descriptor.SearchRegularExpression = True
        if controller.Selection.supportsService("com.sun.star.text.TextTableCursor"):
            table = doc.getTextTables().getByName(doc.CurrentController.ViewCursor.TextTable.Name)
            rangeName = doc.CurrentController.Selection.getRangeName()
            for i in range(int(rangeName[1]), int(rangeName[4]) + 1):
                for x in range(ord(rangeName[0]), ord(rangeName[3]) + 1):
                    cell = table.getCellByName(f'{chr(x)}{i}')
                    start = cell.Text.createTextCursorByRange(cell.Start)
                    end = cell.Text.createTextCursorByRange(cell.End)
                    if doc.Text.compareRegionEnds(start, end) >= 0:
                        start.goRight(0, False)
                        found = doc.findNext(start, search_descriptor)
                        while found:
                            if doc.Text.compareRegionEnds(found, end) == -1:
                                break
                            found.CharColor = CharColor
                            found = doc.findNext(found.End, search_descriptor)
        if controller.Selection.supportsService("com.sun.star.text.TextRanges"):
            selection = doc.getCurrentController().getSelection().getByIndex(0)
            start = selection.Text.createTextCursorByRange(selection.Start)
            end = selection.Text.createTextCursorByRange(selection.End)
            if doc.Text.compareRegionEnds(start, end) >= 0:
                start.goRight(0, False)
                found = doc.findNext(start, search_descriptor)
                while found:
                    if doc.Text.compareRegionEnds(found, end) == -1:
                        break
                    found.CharColor = CharColor
                    found = doc.findNext(found.End, search_descriptor)
        return ()


class DocumentHandler(unohelper.Base, XDocumentHandler):
    def __init__(self, element, colorlist):
        self.element = element
        self.colorlist = colorlist
    def startDocument(self): pass
    def endDocument(self): pass
    def startElement(self, aName: str, xAttribs: list):
        if aName != "draw:color":
            pass
        n = xAttribs.getValueByName("draw:name")
        v = xAttribs.getValueByName("draw:color")
        if n and v:
            self.colorlist.append((n, v))
    def endElement(self, aName: str): pass
    def characters(self, aChars: str): pass
    def ignorableWhitespace(self, aWhitespaces: str): pass
    def processingInstruction(self, aTarget: str, aData: str): pass
    def setDocumentLocator(self, xLocator): pass

class AdjustmentListener(unohelper.Base, XAdjustmentListener):
    def __init__(self, colortable):
        self.colortable = colortable
    def adjustmentValueChanged(self, rEvent):
        self.colortable.setPosSize(0, -rEvent.Value, 0, 0, Y)

class RGBFieldListener(unohelper.Base, XTextListener):
    def __init__(self, parent):
        self.parent = parent
    def textChanged(self, event):
        default_color = self.parent.getControl("defaultColor")
        r, g, b = (
            int(self.parent.getControl("R_color").Value),
            int(self.parent.getControl("G_color").Value),
            int(self.parent.getControl("B_color").Value))
        default_color.setDesignMode(True)
        default_color.Model.BackgroundColor = RGB(r, g, b)
        default_color.Model.HelpURL = f"#{r:02x}{g:02x}{b:02x}"
        default_color.setDesignMode(False)

class DefaultColorListener(unohelper.Base, XMouseListener):
    def __init__(self, name, parent):
        self.name = name[1:]
        self.default = parent.getControl("defaultColor")
        self.R = parent.getControl("R_color")
        self.G = parent.getControl("G_color")
        self.B = parent.getControl("B_color")
    def mousePressed(self, event):
        self.R.setText(int(self.name[:2], 16))
        self.G.setText(int(self.name[2:4], 16))
        self.B.setText(int(self.name[4:6], 16))
    def mouseReleased(self, event): pass
    def mouseEntered(self, event): pass
    def mouseExited(self, event): pass

class SettingsDialogJob(unohelper.Base, XJob):
    def __init__(self, ctx):
        self.ctx = ctx
        self.colorlist = []
        self.readColorSettings()
    def readColorSettings(self):
        sxmlURL = createUnoService(self.ctx, "com.sun.star.util.PathSubstitution").substituteVariables(f"{os.path.dirname(__file__)}/dialogs/standard.soc",True)
        oSFA = createUnoService(self.ctx, "com.sun.star.ucb.SimpleFileAccess")
        if not oSFA.exists(sxmlURL):
            print("not exist")
            return
        oInput = oSFA.openFileRead(sxmlURL)
        oParser = createUnoService(self.ctx, "com.sun.star.xml.sax.Parser")
        oParser.setDocumentHandler(DocumentHandler(oParser, self.colorlist))
        oInputSource = uno.createUnoStruct("com.sun.star.xml.sax.InputSource")
        oInputSource.aInputStream = oInput
        oParser.parseStream(oInputSource)
        oInput.closeInput()
        return

    def addAwtModel(self, mtype, name, x, y, width, height, props):
        control = self.ctx.ServiceManager.createInstance(f"com.sun.star.awt.UnoControl{mtype}")
        control_model = self.ctx.ServiceManager.createInstance(f"com.sun.star.awt.UnoControl{mtype}Model")
        for k, v in props.items():
            control_model.setPropertyValue(k, v)
        control.setModel(control_model)
        control.setPosSize(x, y, width, height, POSSIZE)
        return control, control_model

    def getOoLocale(self):
        ooLocal = {
            'en': {"CheckBox": "Color Tashkel", "edit": "Edit", "Recent": "Recent"},
            'ar': {"CheckBox": "تلوين التشكيل", "edit": "حرر", "Recent": "الأخيره"},
            'fr': {"CheckBox": "Colorer techkil", "edit": "Éditer", "Recent": "Récente"},
            'de': {"CheckBox": "Tashkil kolorieren", "edit": "Edit", "Recent": "Recent"}
        }
        L10Ncfg = self.ctx.ServiceManager.createInstanceWithContext(
            "com.sun.star.configuration.ConfigurationProvider", self.ctx)
        nodepath = PropertyValue(Name="nodepath", Value="/org.openoffice.Setup/L10N")
        code = L10Ncfg.createInstanceWithArguments("com.sun.star.configuration.ConfigurationAccess", (nodepath,))
        olocale = ooLocal.get(code.getByName("ooLocale")[:2], ooLocal.get('ar'))
        return olocale

    def createdialog(self):
        dialog, dialog_model = self.addAwtModel("Dialog", "Dialog", 0, 0, 370, 300, {})
        default_settings = {}
        with open(f"{os.path.dirname(__file__)}/dialogs/settings.txt", "r") as settings:
            for line in settings.readlines():
                key, value = line.strip().split(":")
                default_settings.update({key: value})

        allAwtModel = (
            ('CheckBox', 'CheckBox', 10, 10, 150, 25,
             dict(Label=self.getOoLocale()['CheckBox'], State=int(default_settings["CheckBox"]))),
            ('FixedText', 'defaultColor', 240, 50, 118, 30,
             dict(
                 BackgroundColor=int(default_settings["rgb"]),
                 HelpURL=default_settings["hex"],
                 Border=2)),
            ('NumericField', 'R_color', 240, 100, 120, 10,
             dict(
                 DecimalAccuracy=0,
                 Repeat=True,
                 RepeatDelay=50,
                 Spin=True,
                 ValueMax=255,
                 ValueMin=0,
                 Value=int(default_settings["hex"].strip("#")[:2], 16))),
            ('NumericField', 'G_color', 240, 135, 120, 10,
             dict(
                 DecimalAccuracy=0,
                 Repeat=True,
                 RepeatDelay=50,
                 Spin=True,
                 ValueMax=255,
                 ValueMin=0,
                 Value=int(default_settings["hex"].strip("#")[2:4], 16))),
            ('NumericField', 'B_color', 240, 170, 120, 10,
             dict(
                 DecimalAccuracy=0,
                 Repeat=True,
                 RepeatDelay=50,
                 Spin=True,
                 ValueMax=255,
                 ValueMin=0,
                 Value=int(default_settings["hex"].strip("#")[4:6], 16))),
            ('Button', 'okButton', 10, 255, 120, 35, {'DefaultButton': True, 'PushButtonType': 1}),
            ('Button', 'cancelButton', 135, 255, 120, 35, {'PushButtonType': 2}),
            ("Container", "base", 10, 50, 220, 150, {})
        )
        rgb_listener = RGBFieldListener(dialog)
        for awt_model in allAwtModel:
            control, control_model = self.addAwtModel(*awt_model)
            if awt_model[0] == "NumericField":
                # print(dir(control))
                control.addTextListener(rgb_listener)
            dialog.addControl(awt_model[1], control)

        containers = (
            ("Container", "colortable", 4, 0, 200, int(len(self.colorlist) / 8) * 25, dict(BackgroundColor=16777215)),
            ("ScrollBar", "scrollbar", 207, 0, 13, 150,
             dict(
                 Orientation=1,
                 Border=1,
                 LiveScroll=True,
                 LineIncrement=25,
                 BlockIncrement=25,
                 VisibleSize=25,
                 ScrollValueMax=int(len(self.colorlist) / 8) * 24)
             )
        )
        colortable = scrollbar = None
        for container in containers:
            control, control_model = self.addAwtModel(*container)
            if container[1] == "colortable":
                colortable = control
            if container[1] == "scrollbar":
                scrollbar = control
            dialog.getControl("base").addControl(container[1], control)

        col, row = 0, 0
        nsize = 24
        count = 0
        for color in self.colorlist:
            control, control_model = self.addAwtModel("FixedText", "", (nsize + 1) * col, (nsize + 1) * row + 1, nsize,
                                                      nsize,
                                                      dict(
                                                          BackgroundColor=int(color[1][1:], 16),
                                                          Border=2, HelpText=color[0], HelpURL=color[1])
                                                      )
            control.addMouseListener(DefaultColorListener(color[1], dialog))
            colortable.addControl(f"{color[1]}", control)
            col += 1
            count += 1
            if col > 7:
                col = 0
                row += 1
        adj = AdjustmentListener(dialog.getControl("base").getControl("colortable"))
        scrollbar.addAdjustmentListener(adj)
        return dialog

    def execute(self, args):
        dialog = self.createdialog()
        dialog.setVisible(True)
        execute = dialog.execute()
        if execute == 1:
            with open(f"{os.path.dirname(__file__)}/dialogs/settings.txt", "w+") as settings:
                settings.write(f"CheckBox:{dialog.getControl('CheckBox').State}\n")
                settings.write(f"rgb:{dialog.getControl('defaultColor').Model.BackgroundColor}\n")
                settings.write(f"hex:{dialog.getControl('defaultColor').Model.HelpURL}\n")
        return ()


class MenuDispatch(unohelper.Base, XDispatch):
    def __init__(self, ctx, args):
        self.ctx = ctx
        self.frame = args
        self.toolkit = ctx.getServiceManager().createInstanceWithContext("com.sun.star.awt.Toolkit", ctx)
    def dispatch(self, url, args):
        try:
            if url.Path == "settings":
                job = SettingsDialogJob(self.ctx)
                job.execute(())
        except Exception as e:
            print(e)
        finally:
            return
    def addStatusListener(self, listener, url): return
    def removeStatusListener(self, listener, url): return


class ButtonDispatch(unohelper.Base, XDispatch):
    def __init__(self, ctx, args):
        self.ctx = ctx
        self.frame = args
        self.desktop = self.ctx.ServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", self.ctx)
        self.toolkit = ctx.getServiceManager(). \
            createInstanceWithContext("com.sun.star.awt.Toolkit", ctx)
        self.ooLocal = self.getOoLocale()
        self.listener = None

    def dispatch(self, url, args):
        try:
            if url.Path == "ToggleDropdownButton":
                if args[1].Name == "Text" and args[1].Value:
                    if  args[1].Value == self.ooLocal[0]:
                            job = RemoveTashkelJob(self.ctx)
                            job.execute(())
                    if args[1].Value == self.ooLocal[1]:
                            job = ColorTashkelJob(self.ctx)
                            job.execute(())
                else:
                    job = Tashkel(self.ctx)
                    job.execute(())
        except Exception as e:
            print(e)
        finally:
            aContextMenu = uno.Any('[]string', self.ooLocal)
            aArgs = makeNamedValue('List', aContextMenu)
            self.sendCommandTo(url, "SetList", (aArgs,))
            return

    def addStatusListener(self, listener, url):
        aContextMenu = uno.Any('[]string', self.ooLocal)
        args = makeNamedValue('List', aContextMenu)
        self.sendCommandTo(url, "SetList", (args,), listener)
        self.listener = listener
        return

    def removeStatusListener(self, listener, url): return

    def getOoLocale(self):
        ooLocal = {'en': ('Remove Tashkel', 'Color Tashkel'),
                   'ar': ('إزالة التشكيل', 'تلوين التشكيل'),
                   'fr': ('Enlever techkil', 'Colorer techkil'),
                   'de': ('Tashkil entfernen', 'Tashkil kolorieren')
                   }
        L10Ncfg = self.ctx.ServiceManager.createInstanceWithContext(
            "com.sun.star.configuration.ConfigurationProvider", self.ctx)
        nodepath = PropertyValue(Name="nodepath", Value="/org.openoffice.Setup/L10N")
        code = L10Ncfg.createInstanceWithArguments("com.sun.star.configuration.ConfigurationAccess", (nodepath,))
        olocale = ooLocal.get(code.getByName("ooLocale")[:2], ooLocal.get('ar'))
        return olocale

    def sendCommandTo(self, url, cmd, args, xControl=None, Enabled=True):
        aEvent = FeatureStateEvent()
        aControlCmd = ControlCommand()
        aControlCmd.Command = cmd
        aControlCmd.Arguments = args
        aEvent.State = aControlCmd
        aEvent.FeatureURL = url
        aEvent.IsEnabled = Enabled
        aEvent.Requery = False
        aEvent.Source = self
        if xControl:
            xControl.statusChanged(aEvent)
            return
        self.listener.statusChanged(aEvent)
        return

    def document(self): return self.desktop.getCurrentComponent()


class Handler(unohelper.Base, XDispatchProvider, XInitialization):
    def __init__(self, ctx):
        self.ctx = ctx
        self.frame = None
        self.smgr = self.ctx.getServiceManager()
        self.toolkit = self.smgr.createInstanceWithContext("com.sun.star.awt.Toolkit", ctx)

    def initialize(self, objs):
        if len(objs) > 0:
            self.frame = objs[0]
        return

    def queryDispatch(self, url, target, searchflags):
        if url.Protocol == "Button:":
            dispatch = self.smgr.createInstanceWithArgumentsAndContext(
                "LibreOffice.ProtocolHandler.ButtonDispatch",
                (self.frame, ), self.ctx)
            return dispatch
        if url.Protocol == "Menu:":
            dispatch = self.smgr.createInstanceWithArgumentsAndContext(
                "LibreOffice.ProtocolHandler.MenuDispatch",
                (self.frame, ), self.ctx)
            return dispatch
        return None

    def queryDispatches(self, requests):
        result = []
        for item in requests:
            result.append(self.queryDispatch(item.FeatureURL, item.FrameName, item.SearchFlags))
        return tuple(result)


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(
    Handler,
    "LibreOffice.ProtocolHandler.MishkalHandler",
    ("com.sun.star.frame.ProtocolHandler",), )
g_ImplementationHelper.addImplementation(
    MenuDispatch,
    "LibreOffice.ProtocolHandler.MenuDispatch",
    ("com.sun.star.frame.XDispatch",),)
g_ImplementationHelper.addImplementation(
    ButtonDispatch,
    "LibreOffice.ProtocolHandler.ButtonDispatch",
    ("com.sun.star.frame.XDispatch",),)
